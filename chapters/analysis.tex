\chapter{Empirical Analysis}
To assess the prevalence of the patterns our tool attempts to rewrite, we perform an empirical analysis on a data set of 1000 Cordova applications downloaded from the AndroZoo collection \cite{Alli16a}. We acquired the data set by downloading applications at random, checking if they are apps built with the Cordova framework, and repeating this process until we had gathered 1000 APKs in total. The downloading process was constrained by two conditions: First, we only downloaded applications originating from the Google Play Store. Second, we ignored any applications with a dexdate older than three years. We then decompiled all the APKs, and ran our tool against the resulting source codes. 175 applications could not be parsed properly, which left us with metrics for 825 apps.\\

Statistics were generated for all Javascript APIs that our tool attempts to rewrite. We analyze all statistics once with all files included and once with files excluded that have either ``cordova'', ``jquery'', ``angular'' or ``ionic'' in their names. We consider such files belonging to the respective third-party libraries and exclude them, because we cannot consider them to be under full control of the developers. Developers may not want to change such files because they are subject to change when updating an app's dependencies, and the developers of these libraries may deal with CSP-related refactorings on their own. It can therefore make sense to exclude those files from our analysis when assessing the effectiveness of our tool. The results of the empirical analysis are presented in this chapter.

\section{Occurrence of Impacted APIs}
First we analyzed the overall occurrence of the Javascript APIs affected by Content Security Policy, thus answering our fourth research question: ``How prevalent are the patterns we attempt to rewrite?''\\

As can be seen in table 6.1, prevalence differs greatly between the individual APIs. While either one of the ``setTimeout'' or ``setInterval'' methods was present in almost all of the analyzed applications, the ``insertRule'' APIs are barely being used. The high occurrence of the ``eval'' function is notable, since it negatively impacts both performance and security of an application, as Richards et al. \cite{Rich11a} have discussed.\\

When we exclude all files belonging to one of the commonly found third-party libraries, we can see that those make a big difference for the ``eval'', ``setTimeout''/``setInterval'', ``Function'' and ``cssText'' APIs and a slightly smaller one for the ``setAttribute'' API when used to set inline style attributes. This means that a significant number of applications make no use of those APIs in their actual application code and policies are often impaired because of the libraries an application uses.


\begin{table}[h!]
\centering
 \begin{tabular}{||c c c||} 
 \hline
   API & \% Apps & \% Apps (w/o library files) \\ [0.5ex] 
 \hline\hline
   setAttribute (event handler) & 3.88 & 3.31 \\
 \hline
   eval & 89.45 & 36.89 \\
 \hline
   setTimeout / setInterval & 98.30 & 73.65 \\
 \hline
   Function & 61.21 & 29.41 \\
 \hline
   setAttribute (style) & 30.91 & 21.45 \\ 
 \hline
   cssText & 83.27 & 28.68 \\
 \hline
   insertRule & 4 & 4 \\
 \hline
\end{tabular}
\caption{Apps containing CSP-relevant APIs}
\label{table:1}
\end{table}


\section{Constant vs. Non-Constant Parameters}
In table 6.2, we can see how exactly the CSP-relevant APIs are used amongst the applications in our data set. It gives an answer to our research questions 4.1 (``How prevalent are the patterns we can successfully rewrite?'') and 4.2 (``How prevalent are the patterns we cannot rewrite?''). With the exception of the ``cssText'' API and the ``setAttribute'' API when used to add inline event handlers, usage with non-constant parameters (i.e. where the values of the parameters is not available from the immediate context, see section \ref{variable assignments} for details) clearly dominates when compared to usage with non-constant ones. These relations do not change when excluding library files, as can be seen in table 6.3. We can however tell that the share of API calls coming from library files varies greatly depending on the specific API we investigate. It appears that almost a third of the non-constant ``eval'' invocations, almost two thirds of the ``setTimeout''/``setInterval'' and ``cssText'' invocations and more than half of the ``Function'' invocations come from either one of the four libraries. To clarify this point, table 6.4 shows the percentage of each API usage coming from a file belonging to one of the libraries. This means that while it is important that app developers choose their APIs wisely, they are often forced to weaken their CSP definitions by consequence of using certain third-party libraries. From the high share of constant ``cssText'' usage among libraries we can further conclude that library developers could easily refactor parts of their code to make it more conforming to CSP's requirements. Of course, this must be an ``all or nothing'' decision; unless all (i.e. constant and non-constant) invocations of ``cssText'' and ``insertRule'' are rewritten, developers using a library cannot refrain from adding ``style-src unsafe-eval'' to their CSP definitions.\\

In addition to the abovementioned metrics, there are two numbers we have collected that are not included in the referenced tables. First, we have found exactly zero (constant) usages of the ``eval'' method to parse JSON strings. This is noteworthy because according to Richards et al. \cite{Rich11a}, this type of ``eval'' usage was quite common some years ago. Second, there are 36124 invocations of the ``setTimeout''/``setInterval'' APIs that do not rely on string evaluation and are therefore not relevant for Content Security Policy. They shall be mentioned here for completeness' sake.


\begin{table}[h!]
\centering
 \begin{tabular}{||c c c||} 
 \hline
   API & Constant parameters & Non-constant parameters \\ [0.5ex] 
 \hline\hline
   setAttribute (event handler) & 32 & 7 \\
 \hline
   eval & 972 & 2448 \\
 \hline
   setTimeout / setInterval & 427 & 16449 \\
 \hline
   Function & 93 & 1451 \\
 \hline
   setAttribute (style) & 424 & 895 \\ 
 \hline
   cssText & 4209 & 2355 \\
 \hline
   insertRule & 0 & 87 \\
 \hline
\end{tabular}
\caption{Number of CSP-relevant API calls}
\label{table:2}
\end{table}

\begin{table}[h!]
\centering
 \begin{tabular}{||c c c||} 
 \hline
   API & Constant parameters & Non-constant parameters \\ [0.5ex] 
 \hline\hline
   setAttribute (event handler) & 27 & 6 \\
 \hline
   eval & 971 & 1692 \\
 \hline
   setTimeout / setInterval & 308 & 6056 \\
 \hline
   Function & 78 & 696 \\
 \hline
   setAttribute (style) & 417 & 756 \\ 
 \hline
   cssText & 905 & 779 \\
 \hline
   insertRule & 0 & 85 \\
 \hline
\end{tabular}
\caption{Number of CSP-relevant API calls, library files excluded}
\label{table:3}
\end{table}

\begin{table}[h!]
\centering
 \begin{tabular}{||c c c||} 
 \hline
   API & \% Constant parameters & \% Non-constant parameters \\ [0.5ex] 
 \hline\hline
   setAttribute (event handler) & 15.63 & 14.23 \\
 \hline
   eval & 0.1 & 30.88 \\
 \hline
   setTimeout / setInterval & 27.87 & 63.18 \\
 \hline
   Function & 16.13 & 52.03 \\
 \hline
   setAttribute (style) & 1.65 & 15.53 \\ 
 \hline
   cssText & 78.5 & 66.92 \\
 \hline
   insertRule & - & 2.3 \\
 \hline
\end{tabular}
\caption{Percentage of CSP-relevant APIs coming from library files}
\label{table:4}
\end{table}



\section{Limitations to Rewriting}
As could already be suspected from the high numbers of non-constant API invocations in the section above, our approach to rewriting CSP-relevant APIs often fails to have any effect on the eventual CSP definitions for an application. Table 6.5 shows the percentage of applications our tool was forced to add a strictness-weakening keyword to because they included non-constant API calls. As shown, the vast majority of ``unsafe-eval'' keywords for both the ``script-src'' and ``style-src'' directives could not be prevented by our tool. The percentage of applications that actually benefitted from our application is shown in table 6.6. As explained in the previous chapter, only small applications that do not make much use of UI libraries could successfully be rewritten in order to prevent less strict CSP definitions. The results of our empirical analysis confirm this: While our rewriting efforts removed the need for ``unsafe-inline'' for the ``style-src'' directive in 8\% of apps in our data set, the shares for the other keywords are significantly lower. Also, excluding the third-party library files has not much impact on those ratios. The empirical analysis therefore backs up our preliminary and mostly negative answer to the third research question (``Can we automatically generate sensible CSP definitions for real-world Cordova apps?'').\\

What must be considered when reviewing those metrics is the fact that for some keywords, there are many applications that did not need any rewriting at all. For example, the ``script-src: unsafe-inline'' policy was necessary for 0.61\% of applications, could be avoided for a further 3.27\% of them, and never even had to be considered for the remaining 96.12\%. When putting our statistics in relation to the number of apps that had an actual need for rewriting, we get metrics for how effective our rewriting approach is. These metrics are shown in table 6.7. There we see that our tool performed quite well for the ``script-src: unsafe-inline'' policy, where 84.28\% of the applications that needed rewriting could benefit. With 25.88\%, a much lower but still significant ratio of apps that needed rewriting could benefit with respect to the ``style-src: unsafe-inline'' policy. From that we can conclude that the hashing approach to the ``script-src'' directive is very effective and that for most applications, there is no need at all to specify ``unsafe-inline'' for scripts --- a practice that is still very widespread amongst developers, according to Weichselbaum et al. \cite{Weic16a} and Willocx et al. \cite{Will17a}. Conversely, our tool was almost completely ineffective with respect to the ``unsafe-eval'' keywords for both the ``script-src'' and ``style-src'' directives.


\begin{table}[h!]
\centering
 \begin{tabular}{||c c c||} 
 \hline
   Keyword & \% Apps & \% Apps (w/o library files) \\ [0.5ex] 
 \hline\hline
   script-src: unsafe-inline & 0.61 & 0.49 \\
 \hline
   script-src: unsafe-eval & 98.55 & 79.17 \\
 \hline
   style-src: unsafe-inline & 22.91 & 13.48 \\
 \hline
   style-src: unsafe-eval & 81.33 & 26.96 \\
 \hline
\end{tabular}
\caption{Apps requiring extra keywords because they could not be rewritten}
\label{table:5}
\end{table}


\begin{table}[h!]
\centering
 \begin{tabular}{||c c c||} 
 \hline
   Keyword & \% Apps & \% Apps (w/o library files) \\ [0.5ex] 
 \hline\hline
   script-src: unsafe-inline & 3.27 & 2.82 \\
 \hline
   script-src: unsafe-eval & 0 & 0.12 \\
 \hline
   style-src: unsafe-inline & 8 & 7.97 \\
 \hline
   style-src: unsafe-eval & 2.42 & 2.7 \\
 \hline
\end{tabular}
\caption{Apps successfully rewritten to avoid keywords}
\label{table:6}
\end{table}

\begin{table}[h!]
\centering
 \begin{tabular}{||c c c||} 
 \hline
   Keyword & \% Apps & \% Apps (w/o library files) \\ [0.5ex] 
 \hline\hline
   script-src: unsafe-inline & 84.28 & 85.2 \\
 \hline
   script-src: unsafe-eval & 0 & 0.15 \\
 \hline
   style-src: unsafe-inline & 25.88 & 37.16 \\
 \hline
   style-src: unsafe-eval & 2.89 & 9.1 \\
 \hline
\end{tabular}
\caption{Apps successfully rewritten in relation to apps that need rewriting}
\label{table:7}
\end{table}



\section{Framework Usage}
We also analyzed the names of all Javascript files loaded into applications in our data set. We found three third-party libraries to be common among the applications we analyzed: The jQuery library, which is the most common one with almost two thirds of apps making use of it. The Angular framework, which is present in nearly a quarter of applications. And the Ionic framework which has similar prevalence as the Angular framework. Table 6.7 shows these results in detail.\\

For the jQuery library, many files include a version string inside their name, which we also analyzed. We found that even though our data set only includes applications no older than three years, there is a striking tendency towards older versions of the library. These results can be seen in table 6.8. Out of the 489 jQuery files that include a version string, the large majority are files from jQuery version 1, with about half that amount from version 2 and only a twentieth using the latest version 3. Moreover, the latest minor versions used are 1.9 (highest available 1.12), 2.2 (which is the highest available) and 3.3 (highest availble 3.4), with their last updates received in February 2013, May 2016 and January 2018, respectively. This means that over 60\% of the jQuery files found in the data set are more than 6 years old with almost another third being older than 3 years. This is especially relevant because jQuery uses the ``eval'' method internally, which might be subject to change in future versions of the library. But since adoption of new versions is obviously very low, this may not have an impact on CSP definitions for Cordova applications in the near to mid-term future.

\begin{table}[h!]
\centering
 \begin{tabular}{||c c||} 
 \hline
   Library & Occurrence[\%] \\ [0.5ex] 
 \hline\hline
   jQuery & 64.36 \\
 \hline
   Angular & 24.12 \\
 \hline
   Ionic & 23.27 \\
 \hline
\end{tabular}
\caption{Percentage of apps including third-party libraries}
\label{table:7}
\end{table}


\begin{table}[h!]
\centering
 \begin{tabular}{||c c||} 
 \hline
   Version & Occurrence[\%] \\ [0.5ex] 
 \hline\hline
   1.x.x & 64.01 \\
 \hline
   2.x.x & 29.86 \\
 \hline
   3.x.x & 5.11 \\
 \hline
\end{tabular}
\caption{Percentage of files including jQuery versions}
\label{table:8}
\end{table}

\section{Threats to Validity}
The analysis we performed does have some limitations inherent to its approach. First, the parsing of HTML files may have failed in some cases due to non-standard syntax being used. If this happens, our tool just skips the violating file and proceeds, effectively causing any Javascript snippets related to the file to be skipped as well. This can cause our metrics to be lower than they actually are.\\

Second, we assessed whether files belong to a third-party library like jQuery or Angular based on their names. Specifically, we looked for (case-insensitive) occurrences of the strings ``cordova'', ``jquery'', ``angular'' or ``ionic''. Since we cannot rule out the fact that some files are part of a third-party library but do not include such strings in their name, there might be some false negatives present in those metrics. Furthermore, developers may have named some of their files to include such a string; e.g. because they contain a part of their codebase that interfaces with one of those libraries. In this case, there could be false positives present. Finally, there might be other large third-party libraries we have not considered in our analysis; though we have not found any indication of this fact. In this case, our conclusions about the impact of third-party libraries on CSP definitions might be incomplete.

