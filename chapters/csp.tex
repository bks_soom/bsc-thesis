\chapter{Content Security Policy}
Content Security Policy is a security mechanism directed against certain classes of attacks, the most notable ones being code injection and cross-site scripting. It is implemented in all modern browsers with the first version of the standard having been published by W3C in 2012. The currently recommended version is CSP level 2, with its third iteration being a working draft. Although not yet completed, many features of CSP level 3 are already supported by major browsers like Chrome (and Android's WebView as well, since it is based on Chrome). In the following chapter, Content Security Policy is described both in terms of its mechanisms and with respect to mobile web applications and the various attacks mentioned in the previous chapter. 

\section{How CSP Works}
The main purpose of Content Security Policy is to restrict the resources a given application is allowed to load from or connect to, as well as limiting the execution of certain types of code. This is achieved by sending a CSP definition along with every page that is loaded by the browser engine. Such a definition can take the form of an HTTP header (called ``Content-Security-Policy'') or an HTML <meta> tag which is part of the loaded document's <header> tag. A complete CSP definition consists of one or multiple policy directives, each of which restricts certain aspects of an application's behavior.\\

A policy directive is a string of text whose first word is the name of the directive while the subsequent words represent the (classes of) sources which are allowed for this given directive. Policy directives are whitelists, meaning that any resource not included in the directive is fully disallowed. For example, a policy directive for limiting images and icons can look like this: ``img-src `self' https://img-example.ch data:;''. This specific definition prevents any <img> or <link rel=``icon''> tags from loading pictures from any source that is not either local (``self''), the host ``img-example.ch'' reached via a HTTPS connection, or a valid ``data:'' URI. The semicolon marks the end of a policy directive and is necessary to seperate multiple directives. It can be omitted if no other directives follow. The specifics of how Content Security Policy works are described in the following section.

\subsection{CSP Sources}
The sources to be declared for a given directive can take one of five forms, depending on which directive they are specified for.

\subsubsection*{Default Sources}
For most directives, a source can be either a host-source, a scheme-source, the string ``self'' or the string ``none''.\\

A host-source is either a domain name or an IP address and can optionally include a URL scheme or a port number. Both the port number and subdomains can be specified using the wildcard ``*'', which allows any value in that position. Examples for host-sources are:
\begin{itemize}[itemsep=1pt]
  \item example.ch
  \item https://example.ch:3000
  \item ftp://*.example.ch
  \item sub.example.ch:*
\end{itemize}

A scheme-source consists of only a URL scheme name like ``http:'' or ``ftp:''. Data schemes like ``data:'', ``mediastream:'', ``blob:'' and ``filesystem:'' can be used as well. The colon is mandatory and quotation marks must not be used. Such a scheme-source whitelists any source that uses the given URL scheme. Examples for scheme-sources are the aforementioned strings. Because there have been various security issues with ``data:'' URLs, developers are discouraged from specifying them in CSP directives, especially for the ``script-src'' directive.\\

The strings ``self'' and ``none'' are special values which must be enclosed in single quotation marks. While ``self'' whitelists all local resources (i.e. all resources with the same origin as the current page), ``none'' is the empty set which matches no resource at all. In some browsers, ``blob:'' and ``filesystem:'' resources are not included in ``self''.

\subsubsection*{Special Sources for ``style-src'' and ``script-src''}
In addition to the possible sources that follow the default form, the ``script-src'' and ``style-src'' directives allow four more strings to be declared. Those are ``unsafe-inline'', ``unsafe-eval'', ``nonce-<base64>'' and ``<hashing-algorithm>-<base64>''. For all of them, single quotation marks are necessary.\\

The ``unsafe-inline'' keyword allows the application of inline <style> tags and inline style attributes in case of the ``style-src'' directive. It can also be declared for the ``script-src'' directive, in which case it allows the execution of inline <script> tags and inline event handlers. The ``unsafe-eval'' string allows the execution of string evaluation methods for stylings, such as the ``insertRule'' method or assignments to the ``cssText'' attribute. When declared for the ``script-src'' directive, this keyword allows string evaluation for scripts, such as the ``eval'' or ``window.setTimout'' methods. A list of APIs that are affected by ``unsafe-inline'' and ``unsafe-eval'' can be found below in the descriptions of the ``script-src'' and ``style-src'' directives.\\

The nonce and hash keywords are alternatives to ``unsafe-inline'', meaning that if a hash or a nonce is present alongside the ``unsafe-inline'' rule, the latter will be ignored. Nonces and hashes can be used to give trust to specific <style> or <script> tags. To use a nonce, a unique, base64-encoded value must be generated with every request and put into either the ``style-src'' or ``script-src'' directive. Furthermore, the ``nonce'' attribute must be set to that same value on every tag that shall be given trust. When using hashes, a hash value must be generated from the <style> or <script> tag's content. That hash, prefixed by the applied hashing algorithm, must then be put into the ``style-src'' or ``script-src'' directive. Both of these methods will allow those trusted styles / scripts to be applied / executed. Starting with WebView version 59, this method can also be applied to <script> tags that load external Javascript. Examples for the nonce and hash sources are:
\begin{itemize}[itemsep=1pt]
  \item style-src: ``nonce-f65724a6b3'';\\
    to give trust to the tag <style nonce=``f65724a6b3''>
  \item script-src: ``sha256-dd9fe856ba4d31a77ba85df53a0d77ef...'';\\
    to give trust to the tag <script>alert(``hash'');</script>
\end{itemize}

\subsubsection*{Further Special Sources for ``script-src''}
When specifying the ``script-src'' directive, there are three more keywords that can be declared. Those are the ``unsafe-hashes'' keyword, the ``strict-dynamic'' keyword and the ``report-sample'' keyword.\\

The ``unsafe-hashes'' rule along with a hash string as described above, allows the execution of event handlers that match the given hash. The advantage of this method is, that all other forms of inline scripts are still disallowed. The ``strict-dynamic'' keyword allows any script that is being trusted via a nonce or hash to load further scripts which will then receive the same trust themselves. This matches the expected behavior of developers when creating CSP whitelists: If a script from a given source is to be trusted, that source and any source from which further scripts are loaded must be explicitly whitelisted. With ``strict-dynamic'' that tediously recursive task is avoided. If the ``strict-dynamic'' keyword is present, all normal sources present in the ``script-src'' directive (i.e. any sources other than nonces, hashes or ``unsafe-eval'') are ignored. Finally, the ``report-sample'' keyword causes a sample string of any code that violates the declared policies to be included in the violation report generated by CSP.

\subsubsection*{Sources for ``plugin-types''}
The plugin-types directive does not use any of the sources described above. Instead, it accepts a string denoting a MIME type, such as ``application/x-java-applet''.

\subsubsection*{Directives Without Any Sources}
There are some special directives in CSP which stand on their own and do not need declaration of further sources or keywords. Out of the directives relevant for this thesis, only the ``upgrade-insecure-requests'' directive falls in this category.

\subsection{CSP Directives}\label{subsec:CSP directives}
There are currently 30 directives which can be set in a CSP definition. Some of those 30 directives are considered deprecated, while others are still experimental (i.e. not yet part of a finished CSP version). Since not all of those directives are relevant to this thesis, only a selection shall be described here. The directives we consider are those that are supported in WebView versions 52 and above and must be considered relevant for the security of mobile web applications. Directives like ``frame-ancestors'' --- which may be useful, but not directly related to how secure a mobile web application can be --- are omitted. The lower bound of WebView version 52 is chosen, because it is the first version which supports the ``strict-dynamic'' rule, which is essential for a modern definition of the ``script-src'' directive.\\

A more complete reference of CSP directives and their associated rules can be found in the MDN Web Docs \cite{MDNWebDocs} or the latest published version of W3C's working draft for CSP3 \cite{W3CCSP3}. However, not every API affected by a directive may be included in those references. For example, the description of the ``img-src'' directive in the MDN Web Docs does not include the fact that many cases of ``url(...)'' expressions in CSS lead to images being loaded and are therefore restricted by that directive.


\subsubsection*{Relevant Directives That Are Currently Supported}
A comprehensive description of all directives we consider relevant can be found in appendix \ref{chapter:CSP directives}. Only directives that are supported by Android's WebView at the time of writing are included in this list.

\subsubsection*{Relevant Directives That Are Currently Unsupported}
While the abovementioned directives are all supported by current WebView versions, there are two more directives in CSP level 3, which are relevant for WebView security, but not yet supported by the Android WebView.
The first of those directives is ``navigate-to'', which limits the ULRs a document is allowed to initiate navigations to. This can prevent a vulnerable WebView to be navigated to e.g. a phishing site, if an attacker was able to inject code that tries this.
The other relevant directive is ``trusted-types''. This directive restricts the values that known DOM XSS sinks can accept, which strongly reduces the chance of a successful injection attack against the application. The ``trusted types'' API is still under development. More information can be found in an ``Update'' post on Google Developers describing the implementation of this experimental API in Chrome \cite{TrustedTypes}.

\section{How CSP Mitigates Attacks}
The primary goal of Content Security Policy is to provide an additional layer of security against attacks directed at a web application. This is achieved primarily by blocking certain kinds of resources developers do not intend to use. The following section presents the most important ways in which CSP protects or mitigates attacks, thus answering our second research question: ``How can CSP prevent or mitigate attacks?''

\subsection{Preventing Code Injection}\label{subsec:Preventing code injection}
When using a strict set of rules, CSP prevents all untrusted inline scripts and inline event handlers from being executed. This means that an attacker that is able to manipulate the DOM is no longer able to add <script> tags or tags with inline event handlers added to them. Furthermore, string evaluation methods like Javascript's ``eval'' are also blocked from execution by default. Since those are the primary ways of injecting Javascript code into an application, most injections can be prevented using strict CSP. However, if an attacker is able to inject data into the application via a trusted source, code injection is still possible. For mobile web applications, this can pose a risk, since local files almost always need to be able to execute code in such an app. As mentioned by Willocx et al. \cite{Will17a}, this can lead to having to allow e.g. string evaluation for all local files, where a normal web app could specify a CSP definition that only allows string evaluation for a specific external source (where, for example, the jQuery library is hosted). As a consequence of this, an attacker could trick the browser into downloading a malicious file which could then be included in the DOM (see section \ref{Local file inclusion} for details) and would be executed because of CSP allowing the source ``self''.\\

A more modern approach to defining the ``script-src'' directive can however avoid that problem. Since CSP level 2 allows the usage of nonces and hashes to give trust to specific scripts, developers can fully avoid the ``self'' keyword in their ``script-src'' directives. Additionally, this has become significantly easier to achieve with the ``strict-dynamic'' keyword. Using ``strict-dynamic'', developers no longer need to give trust to each script individually, but can propagate trust to scripts loaded from a trusted script, thus greatly reducing the number of nonces / hashes developers need to include in their ``script-src'' definitions. Starting with WebView version 59, hashes and nonces can be used with external scripts as well. For older versions, trust could theoretically be given to such scripts by defining a trusted script that loads those scripts dynamically. However, when we investigated this workaround, it did not yield reliable results. For more details on this workaround, see section \ref{subsubsec:strict-dynamic workaround} \\

\subsection{Preventing the Loading of Arbitrary Data}
By restricting the sources that can be loaded from by <img> tags, ``XMLHttpRequests'' or other methods, CSP prevents attackers from loading arbitrary data into the application. This effectively mitigates cross-site scripting, since the inclusion of remote, attacker-controlled code is not permitted. Furthermore, the HTML tags restricted by ``object-src'' can be used to trick a plugin into executing Javascript code, effectively creating a loophole for code injection attacks developers might think they have closed by blocking inline scripts. 

\subsection{Preventing the Exfiltration of Data}
If an attacker is able to inject code into an application, they might attempt to read and exfiltrate sensitive data such as credentials or personal information. Besides the obvious ``XMLHttpRequest'' and related methods, this can be achieved by creating e.g. an <img> tag which ``loads'' from a URL that consists of an attacker-controlled domain name and the sensitive data included into the URL's query parameters:
\begin{verbatim}
<img src=``https://evil.com?data=someSensitiveData''>
\end{verbatim}
The attacker would then only have to look at their server logs, where an entry from the attempted connection would be present, including the sensitive data inside the query parameters. By restricting the sources for all kinds of HTML tags that create such requests, this exfiltration method can be prevented.

\subsection{Preventing UI Manipulation}
By blocking inline styles and string evaluation for styles, CSP prevents attackers from arbitrarily positioning and styling UI elements in the page. As described in section \ref{UI manipulation}, this could be used to trick the user into entering credentials either into a completely new, trustworthy-looking interface, or into invisible form fields rendered on top of existing ones.

\subsection{Preventing Insecure Connections}
While HTTPS is quite common in today's web, developers might still include HTTP domains in their code by mistake. If such a request is used to load an external script or similar resource, an attacker might perform a man-in-the-middle attack. This would allow them to include malicious data like Javascript code into the response, effectively leading to code injection. By upgrading all HTTP requests to HTTPS when using the ``upgrade-insecure-requests'' directive, CSP can prevent this vector of injection.



