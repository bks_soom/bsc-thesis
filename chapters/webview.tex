\chapter{Anleitung zu wissenschaftlichen Arbeiten}
\section*{WebView}
A WebView is an encapsulation mechanism which allows applying web technologies such as HTML, CSS and Javascript to build installable, possibly offline applications for mobile devices. It is based on a browser engine which renders the given code and displays it to the user. Furthermore, it provides a bridge mechanism that allows the Javascript code to interact with native code, which enables access to device resources critical for many apps, such as the camera, bluetooth functionalities or data from content providers like contacts or call logs.\\

There are several implementations of WebView available, including iOS's ``UIWebView'', Windows Phone OS's ``Windows Browser'' and Android's ``WebView''. The Android WebView, which is the only implementation in the scope of this thesis, is based on the WebKit browser engine for Android versions below 4.4. Starting from Android 4.4, Blink is used as WebView underlying browser engine.\\

Developers can use WebViews either to display some parts of their applications where this might be preferable (e.g. a ``terms and conditions'' page which needs to be updated regularly and easily). Or they can use it to build full-fledged hybrid applications where native code only acts as a wrapper and the app's business logic resides entirely inside a number of WebViews.\\

WebViews have a number of advantages. Most importantly, they significantly enhance portability, since web technologies are platform-agnostic. Hybrid app frameworks like Cordova streamline this by providing appropriate abstractions so developers can easily build apps that support different platforms without having to tailor their software to the specific quirks of their target operating systems. Further advantages include easier distribution of updates and more widespread familiarity with the given technologies.\\

As expected, there are certain downsides to using WebViews. Performance is likely to suffer due to the overhead of the browser engine, which leads to reduced user experience. Also, user interfaces in WebViews tend to violate at least some of the user expectations concerning the look and feel of the applications, since they are used to their platform's design principles which are not enforced within WebViews. Finally, using web technologies comes with a whole class of security problems that native apps would not be affected by, specifically those related to code injection vulnerabilities.\\

\section{The WebView API}
\label{appendix:WebView_API}
The WebView API consists of the main ``WebView'' class as well as multiple classes that are used to configure a given WebView. With respect to security, the most important classes for configuration are the ``WebSettings'' class and the ``WebViewClient'' class. In the following section, the security sensitive APIs of the those classes are discussed. All informations related to the WebView API come from the Android Developers documentation \cite{AndroidDevelopers}.

\subsection{WebView Class}
The main ``WebView'' class is responsible for instantiating a WebView, managing Javascript bridges and loading different types of data in different ways. It also provides a getter for its ``WebSettings'' object, which is described further below.

\subsubsection*{addJavascriptInterface}
With the ``addJavascriptInterface'' method, a Java object can be injected into the given WebView. Apart from this Java object, a name is passed to the method, which can then be used inside the Javascript context of the main frame to access the injected object. For Android versions 4.2 and above, only public methods that are annotated with ``@JavascriptInterface'' can be accessed in this way. Prior versions allow the Javascript code to access all public methods via reflection, including inherited methods. The interactions between Javascript code and injected Java objects happen in a separate, private background thread, which is why thread safety must be maintained at all times.

\subsubsection*{removeJavascriptInterface}
With the ``removeJavascriptInterface'' method, previously injected Java objects can be removed from the WebView. This method was introduced in Android 3.0 to allow for safer handling of Javascript bridges.

\subsubsection*{evaluateJavascript}
The ``evaluateJavascript'' method provides another way of communicating between native Java code and the WebView's Javascript code. It accepts a string containing Javascript code which will be executed in the context of the current page. As a second argument, it will take a callback function which is invoked upon completion of the Javascript code, if this returns a non-null value. The return value of the evaluated code will be passed to the callback function as an argument. According to Song et al. \cite{Song18a}, it was introduced in Android 4.4 to allow Javascript to be executed in the main UI thread, which is not possible with the other APIs.

Compared to the ``addJavascriptInterface'' API, this method of communication between native and web code is more secure, since data is only passed to the provided, trusted Java callback method. Song et al. \cite{Song18a} did not find any vulnerabilities introduced by using this form of communication. It does, however, impose some limitations, since it offers a less direct way of communication compared to the Javascript bridge API. Furthermore, starting with Android 7.0, the state of an empty WebView's Javascript code is not persisted across navigations such as the ``loadUrl'' method. The Android Developers documentation \cite{AndroidDevelopers} recommends using the ``addJavascriptInterface'' API for use cases where persistence is needed across WebView navigations.

\subsubsection*{loadData}
The ``loadData'' method loads a given string via a ``data:'' scheme URL. It accepts a string containing the data to be loaded, one denoting its MIME type and another one denoting its encoding (base64 or URL encoding). Due to the same-origin policy, any Javascript loaded with this method is not able to access content that was loaded using any URL scheme that is not a ``data'' scheme. This includes ``http'' and ``https'' schemes. If such data has to be accessed, the ``loadDataWithBaseURL'' method can be used. Content that was loaded with the ``loadData'' method has an origin of ``null'', which should not be considered a trusted origin, since malicious code is also able to create content that has an origin of ``null''. 

\subsubsection*{loadDataWithBaseURL}
The ``loadDataWithBaseURL'' method is used to load data into the WebView which is then given a specified base URL. This base URL is used for the same-origin policy and for resolving relative URLs. In addition to the parameters the ``loadData'' method accepts, this method also accepts the base URL and a history URL. The latter one is used as a history entry for the loaded content. If the base URL parameter denotes ``http'', ``https'', ``ftp'', ``ftps'', ``about'' or ``javascript'' as the scheme to be used, the loaded content is not able to access local files via ``file'' scheme URLs. If the base URL is not a valid HTTP(S) URL, then the window's origin is set to ``null''. In this case, the same caveats as for the ``loadData'' method apply.

\subsubsection*{loadUrl}
The ``loadUrl'' method loads a given URL into the WebView. Optionally, the method accepts a map of HTTP headers that will be added to the request if provided. As mentioned for the ``evaluateJavascript'' method, calling ``loadUrl'' on an empty WebView destroys the state of any Javascript code previously established in that WebView.

\subsubsection*{postUrl}
Using the ``postUrl'' method, a given URL can be loaded via the HTTP ``POST'' method. The data to be sent in this POST request is passed as a second parameter, alongside the URL that should be loaded.

\subsection{WebSettings Class}
The ``WebSettings'' class holds the state of certain settings for a given WebView. It is derived by calling the ``getSettings'' method of the ``WebView'' class.

\subsubsection*{setJavaScriptEnabled}
This method allows the associated WebView to execute Javascript code. It defaults to a value of ``false''.

\subsubsection*{setMixedContentMode}
The ``setMixedContentMode'' method configures whether or not the associated WebView allows a secure origin to load contents from unsecure origins. The Android Developers documentation \cite{AndroidDevelopers} recommends setting this to never allowing mixed content, which is the default value for Android 5.0 and above. Configuring this to always allow mixed content is ``strongly discouraged'', but also used as the default value for Android 4.4 and below. There is also a third option called the ``compatiblity mode'', which allows certain insecure contents to be loaded, depending on the specific release.

\subsubsection*{setAllowFileAccess}
This method configures whether or not the WebView can access the file system, not including assets and resources of the containing app, which can always be accessed. It defaults to ``true''.

\subsubsection*{setAllowFileAccessFromFileURLs}
The ``setAllowFileAccessFromFileURLs'' method specifies, whether or not Javascript code that was loaded via a file scheme URL is allowed access to content that was loaded via a different file scheme URL. It defaults to ``true'' for Android 4.0.3 and below, while Android 4.1 and above has a default of ``false''. The Android Developers documentation \cite{AndroidDevelopers} recommends setting this to ``false'' in order to enable the most secure configuration. Developers are also advised to explicitly configure this to ``false'' for Android 4.0.3 and below, since it could lead to violation of the same-origin policy. It is noteworthy that this method does not impact resource access for non-Javascript vectors. For example, HTML elements that load resources (such as the <img> tag) are not affected by the restrictions imposed by this setting.

\subsubsection*{setAllowUniversalAccessFromFileURLs}
Similarly to ``setAllowFileAccessFromFileURLs'', this method specifies whether or not Javascript code that was loaded via a file scheme URL is allowed access to content that was loaded from any origin. The same defaults, caveats and recommendations as mentioned for ``setAllowFileAccessFromFileURLs'' apply.

\subsection{WebViewClient Class}
The ``WebViewClient'' class is used to configure how the associated WebView handles certain browsing events. It is created via its constructor and then passed to a WebView using the ``setWebViewClient'' method. This class is relevant for an app's security mostly because it can be used to override WebView's standard behavior when navigation events occur inside the WebView. The default behavior is to create an intent that can be handled by the default browser, which is the recommended way of handling navigation events.

\subsubsection*{shouldInterceptRequest}
This is a callback method that is invoked when a resource request happens and that request is not a ``javascript:'' or ``blob:'' scheme URL and no ``file:'' scheme URL accessing the app's assets or resources. By default, it informs the containing application of the request and allows it to load the contents. This method can be overriden in order to configure the associated WebView's behavior when a resource request is triggered. The Android Developers documentation \cite{AndroidDevelopers} urges developers to use care when accessing private data or the view system, since this method is not called on the UI thread. Starting with Android 4.4, WebViews are based on Blink, which introduces the additional restriction that the ``shouldInterceptRequest'' method is only invoked for valid URLs, but not custom URL schemes.

\subsubsection*{shouldOverrideUrlLoading}
This is a callback method that is invoked when a URL is to be loaded into the associated WebView. If it returns ``false'', the given URL is loaded normally. If it returns ``true'', loading is aborted. By default, the WebView responsible for the URL load asks the Activity Manager to select a handler for the given URL. This method is called for subframes and non-HTTP(S) schemes. It is not invoked for ``POST'' requests. As with ``shouldInterceptRequest'', the ``shouldOverrideUrlLoading'' method is only invoked for valid URLs for Android 4.4 and above.

\subsubsection*{onReceivedSslError}
This is a callback method that is invoked when an SSL error occurs. The method is expected to either cancel or proceed with the loading. The Android Developers documentation \cite{AndroidDevelopers} recommends to always cancel the loading and only override this method for logging or displaying custom error pages. Asking the user how to handle a SSL error is discouraged, since they cannot be expected to make an informed decision on this matter. This method is only invoked on recoverable errors. By default, any loading process that triggers an SSL error is canceled.

\section{Risks Introduced by WebView}
With the current implementation of WebView in Android come some security risks that may violate user privacy and device integrity. The main risks that WebViews are affected by are explained in the following section. It provides a basic overview of the root causes that can lead to a multitude of attacks on WebViews. Such attacks are discussed in detail in chapter \ref{ch:Security analysis} of this thesis.

\subsection{Code Injection}
Since they are based on a browser engine, WebViews share a major security concern with traditional web apps: Mixing application data with executable code. When a WebView is rendered, the browser engine parses the whole HTML content. If it contains valid code (e.g. in a <script> tag), this code is executed. This allows the injection of unexpected Javascript code into the application, if input channels are not sanitized correctly.\\

In traditional web apps, the primary input channel for such attacks is the website itself. An application that allows its users to input some data and then displays it to all the users of the app without properly sanitizing it beforehand, is vulnerable to code injection. An attacker can send valid Javascript code to the application, which then stores this code on the server. If another user visits the app and gets displayed the malicious data, the victim's browser correctly identifies this as code and executes it in the context of the page.\\

In mobile web applications, the number of possible input channels is much greater, as Jin et al. \cite{Jin14a} clearly demonstrate. Besides the common injection methods that also apply to traditional web apps (such as intercepting HTTP connections or leveraging existing UXSS vulnerabilities in the browser engine), mobile web applications can suffer from code injection via device specific resources like contact lists, barcode scanners, file systems and more. The attack surface with respect to code injection is therefore broader in mobile web apps as compared to traditional web apps.\\

Furthermore, the impact of such code injection tends to be more serious in mobile web apps than in traditional web apps. Through WebView's Javascript bridge, the otherwise sandboxed Javascript code in a web page may be given access to sensitive device resources like its location, file system, camera and others.\\

\subsection{Lack of Fine-Grained SOP}
One of the root causes for many attacks against WebViews is the lack of a same-origin policy that is appropriate for common applications of WebView. As Davidson et al. \cite{Davi17a} mention, WebView's same-origin policy falls short in protecting both apps and web contents.\\

For apps, all granted permissions are automatically inherited by any embedded web content, including sub-frames or content loaded from untrusted origins. Furthermore, Java objects injected via the Javascript bridge mechanism are accessible by web content of any origin. Since the Javascript bridge provides often needed functionalities, developers are forced to accept reduced security to achieve these functionalities.\\

For web content, there is no same-origin policy being enforced for interactions from an application to its embedded web content. It is assumed that applications always own the web content that they are embedding. Therefore, malicious applications are not prevented from spying on sensitive web content by same-origin policy.\\

As a consequence of there not being an appropriate same-origin policy, sensitive data and functionality is often left exposed to non-trusted origins. In conjunction with the abovementioned code injection risks, this can lead to attacks that jeopardize user privacy as well as device integrity.


\subsection{Bugs Introduced by WebViews}
The additional layer of abstraction that WebView provides introduces bugs to an application. As Hu et al. \cite{Hu18a} concluded, the causes for such bugs include misalignment of the WebView's lifecycle with that of the enclosing activity or fragment, WebView's quickly evolving nature, wildly varying device configuration, application misconfiguration, misuse of WebView APIs, the error-prone Javascript bridge mechanism, and current limitations in functionality of WebViews compared to full web browsers. While Hu et al. did not analyze the security implications of these classes of bugs, it is likely that some newly-introduced bugs can actually aid attackers in compromising a mobile web application.

\section{Best Practices and Mitigations}
To avoid the most pressing problems arising from WebView, there are some best practices and mitigation techniques that can be applied.  While they cannot solve the core problems that come with using a browser engine to render application content, applying those mitigations can significantly reduce the attack surface of a given app.

\subsection{Use Secure Channels}
To prevent code injection via a man-in-the-middle attack, any contents to be displayed in the WebView should only be loaded over secure channels. This means avoiding HTTP and misconfigured HTTPS.

\subsection{Do not Display Untrusted Contents}
A WebView should not be used to display untrusted content. As the Android Developers documentation \cite{AndroidDevelopers} mentions, WebViews are intended for displaying first-party content that is owned by the app's developers. If third-party content should be displayed, the documentation recommends creating an intent to invoke the default web browser.

\subsection{Sanitize Untrusted Inputs}
If content enters the application from an untrusted source (e.g. the barcode scanner), it should always be sanitized before being displayed inside the WebView. This is especially important for data that is transmitted via the Javascript bridge, since there is no built-in sanitization in place for such transmissions, according to Bai et al. \cite{Bai18a}.

\subsection{Use Safe APIs to Display Untrusted Contents}
If content from untrusted third-parties must be displayed within the WebView, it should only be done using safe APIs that do not execute any Javascript code that might be present inside the data. Examples of unsafe APIs include some of the DOM APIs (such as the ``document.write'' method or the ``innerHTML'' attribute), the jQuery APIs (such as the ``append'' method) as well as other APIs that add data to a WebView's DOM while executing any code contained within.

\subsection{Prevent Navigation}
Since navigating inside a WebView opens up the possibility of loading content that was not intended by developers to be displayed inside the WebView, navigation should generally be disallowed. Clicking links should invoke the default web browser to view such possibly untrusted content.

\subsection{Prevent New Windows and Popups}
As mentioned in the Android Developers documentation \cite{AndroidDevelopers}, WebViews should be prevented from opening new windows and popups. This is to be done by passing ``true'' to the ``setSupportMultipleWindows'' method and not overriding the ``onCreateWindow'' method.

\subsection{Handle SSL Errors Safely}
The ``onReceivedSslError'' method of the ``WebViewClient'' class allows developers to change a WebView's behavior when SSL errors are encountered. This should never be used to ignore SSL errors automatically, since this would effectively allow man-in-the-middle attacks to be performed, which can easily lead to code injection.

\subsection{Use JS Bridges with Care}
The Javascript bridge functionality provided by WebView allows injected Java objects to be accessed by Javascript code from any origin. Furthermore, Javascript bridges often expose sensitive data which should be appropriately protected. Because of those circumstances, Javascript bridges should be used with due care. They should provide only the bare necessities that cannot be avoided for the app to function properly. If possible, they should be completely avoided for Android versions 4.2 or below, since reflection can be used to perform attacks against the WebView. If Javascript bridges are truly necessary, for such versions the ``removeJavascriptInterface'' method should be used in order to destroy any previously injected Java objects after they were used in the intended way.

\subsection{Use Content Security Policy}
Starting with Android 4.4, WebView supports defining a Content Security Policy, a security mechanism supported by most modern browsers that mitigates code injection attacks. A closer description of Content Security Policy, its benefits and how to retrofit applications to support strict policies can be found in the main part of this thesis.

\subsection{Update Regularly}
As with all applications, it is critical to keep WebView versions up-to-date. According to Yang et al. \cite{Yang16a}, WebView was linked to the Android OS before Android 5.0, which meant that WebView could not be updated seperately. Starting with Android 5.0, WebView is its own APK and can be updated independently from the operating system, allowing for a more flexible update strategy.\\

Updates to WebView have in the past included changes that are relevant for application security. With Android 4.2, the @JavascriptInterface annotation was introduced, which fixed a security flaw that enabled Javascript code to access any public method of an injected Java object. With Android 4.4, additional restrictions for custom URL schemes were introduced, which disallowed invoking the ``shouldInterceptRequest'' and ``shouldOverrideUrlLoading'' methods for invalid URLs. Android 4.4 changed WebView's underlying browser engine from WebKit to Blink, which removed universal cross-site scripting (UXSS) vulnerabilities that were present in WebKit. According to Song et al. \cite{Song18a}, Android 5.0 fixed further UXSS vulnerabilities previously present in WebView. Android 6.0 removed availability of the getClass method for Java objects injected via the Javascript bridge. Furthermore, in Android 6.0, the geolocation API was restricted to only be available on secure origins, such as HTTPS.\\

Changes such as those mentioned above have direct consequences for an application's security, since known weaknesses are addressed and risks can be mitigated. A sensible update strategy is therefore needed to avoid known security flaws in WebView.

\subsection{Use Additional Security Measures}
Apart from correctly using the WebView APIs and regularly updating both the operating system and the WebView package, developers can rely on additional security measures to improve the security of their applications.\\

When a hybrid app framework is used, supplementary security mechanisms may be available that can mitigage some of WebView's shortcomings. For example, the Cordova framework allows developers to use several whitelisting mechanisms in order to limit network requests, navigation and intent creation. Willocx et al. \cite{Will17a} have carried out in-depth research on Cordova's security mechanisms and their usages.\\

Futhermore, external tools can be used to either detect present vulnerabilities or provide additional security at runtime. Several previous studies have proposed tools based on static or dynamic analysis, that automatically detect open injection vectors or other vulnerabilities caused by WebView. Refer to chapter \ref{ch:Related work} for studies proposing such tools. Other researchers like Davidson et al. \cite{Davi17a} have implemented services that run alongside any mobile web application and provide secure WebViews that allow extending the access policies provided by WebView. Developers can define so-called ``dynamic access policies'' that enable fine-grained restrictions on resource usage for both native applications and web content, thus protecting benign apps from malicious web content and benign web content from malicious apps. They also implemented a rewriting tool which can make any mobile web application compatible with their service.\\

\chapter{CSP Directives}\label{chapter:CSP directives}
\section{connect-src}
The ``connect-src'' directive limits the resources that can be connected to using certain APIs. Obviously, any API that abstracts away from these interfaces is also restricted by ``connect-src''. An example for this would be the ``jQuery.ajax'' method, which internally uses the ``XMLHttpRequest'' object. The following APIs are restricted by the ``connect-src'' directive:
\begin{itemize}[itemsep=1pt]
  \item HTML's <a> tags using the ``ping'' attribute:\\
    <a ping=``https://example.ch''>
  \item Web API's ``WindowOrWorkerGlobalScope.fetch'' method:\\
    fetch(new Request(``https://example.ch''))
  \item Web API's ``XMLHttpRequest'' object:\\
    new XMLHttpRequest().open(``GET'', ``https://example.ch'')
  \item Web API's ``WebSocket'' object:\\
    new WebSocket(``https://example.ch'')
  \item Web API's ``EventSource'' object:\\
    new EventSource(``https://example.ch'')
  \item Web API's ``navigator.sendBeacon'' method:\\
    navigator.sendBeacon(``https://example.ch'', <data>)
\end{itemize}

\section{default-src}
The ``default-src'' directive is a fallback for all so-called ``fetch directives''. If any of those directives is not defined, the resources specified in ``default-src'' will be used to restrict that directive instead. If a directive is present in the CSP definition, ``default-src'' is completely overriden and will not be taken into consideration for this directive. This means that for the CSP definition ``default-src `self' https://example.ch; img-src `self';'', images are only allowed to load from local resources, even though ``default-src'' specifies ``example.ch'' as a valid resource as well.
The directives for which ``default-src'' will serve as fallback are:
\begin{itemize}[itemsep=1pt]
  \item child-src
  \item connect-src
  \item font-src
  \item frame-src
  \item img-src
  \item manifest-src
  \item media-src
  \item object-src
  \item prefetch-src
  \item script-src
  \item script-src-elem
  \item script-src-attr
  \item style-src
  \item style-src-elem
  \item style-src-attr
  \item worker-src
\end{itemize}

\section{font-src}
The ``font-src'' directive limits the resources which can be loaded via CSS's ``@font-face'' rule: 
\begin{itemize}[itemsep=1pt]
    \item @font-face {src: url(``https://example.ch'');}
\end{itemize}

\section{frame-src}
The ``frame-src'' directive limits the resources which can be loaded in sub-frames:
\begin{itemize}[itemsep=1pt]
  \item HTML's <frame> tag (deprecated):\\
    <frame src=``https://example.ch''>
  \item HTML's <iframe> tag:\\
    <iframe src=``https://example.ch''>
\end{itemize}

\section{img-src}
The ``img-src'' directive limits the loading of images and icons. The following APIs are restricted:
\begin{itemize}[itemsep=1pt]
  \item HTML's <img> tag:\\
    <img src=``https://example.ch''>
  \item HTML's <link> tag with the ``rel'' attribute set to ``icon'':\\
    <link rel=``icon'' href=``https://example.ch''>
  \item CSS rules when they load an image and refer to an external resource:\\
    background-image: url(``https://example.ch'')
\end{itemize}

\section{manifest-src}
The ``manifest-src'' directive limits the resources which can referenced in HTML's <link> tag with the ``rel'' attribute set to ``manifest'':
\begin{itemize}[itemsep=1pt]
  \item <link rel=``manifest'' href=``https://example.ch''>
\end{itemize}

\section{media-src}
The ``media-src'' directive limits the resources from which HTML's media elements can load data:
\begin{itemize}[itemsep=1pt]
  \item HTML's <audio> tag:\\
    <audio src=``https://example.ch''>
  \item HTML's <video> tag:\\
    <video src=``https://example.ch''>
  \item HTML's <track> tag:\\
    <track src=``https://example.ch''>
\end{itemize}

\section{object-src}
The ``object-src'' directive limits the resources from which certain HTML tags associated with browser plugins are allowed to load. The same HTML tags are governed additionally by the ``plugin-types'' directive (see below).
\begin{itemize}[itemsep=1pt]
  \item HTML's <object> tag:\\
    <object data=``https://example.ch''>
  \item HTML's <embed> tag:\\
    <embed src=``https://example.ch''>
  \item HTML's <applet> tag:\\
    <applet archive=``https://example.ch''>
\end{itemize}

\section{script-src}
The ``script-src'' directive is arguably the most important CSP directive. It applies the following restrictions:
\begin{itemize}[itemsep=1pt]
  \item It restricts the allowed sources for external scripts:\\
    <script src=``https://example.ch''>
  \item It restricts the execution of the following inline script variants
    \begin{itemize}[itemsep=1pt]
      \item Inline script tags:\\
        <script>alert(``inline script'')</script>
      \item Inline event handlers:\\
        <div onclick=``alert(`inline event handler')''>  
    \end{itemize}
  \item It restricts the execution of the following string evaluation variants
    \begin{itemize}[itemsep=1pt]
      \item Javascript's ``eval'' method:\\
        eval(`alert(``string evaluation'')')
      \item Javascript's ``Function'' object:\\
        new Function(``factor1'', ``factor2'', ``return factor1*factor2'')
      \item The ``WindowOrWorkerGlobalScope.setTimeout'' method if called with a string argument:\\
        window.setTimeout(`alert(``setTimeout'')', 1000)
      \item The ``WindowOrWorkerGlobalScope.setInterval'' method if called with a string argument:\\
        window.setInterval(`alert(``setInterval'')', 1000)
      \item Some other, non-standard methods of ``WindowOrWorkerGlobalScope'':\\
        WindowOrWorkerGlobalScope.setImmediate()\\
        WindowOrWorkerGlobalScope.execScript()
    \end{itemize}
\end{itemize}

\section{style-src}
Just as the ``script-src'' directive does for Javascript, the ``style-src'' directive enforces several restrictions for CSS rules:
\begin{itemize}[itemsep=1pt]
  \item It restricts the allowed sources for external stylesheets:\\
    <link rel=``stylesheet'' href=``https://example.ch''>
  \item It restricts the allowed sources for CSS's ``@import'' rule:\\
    @import url(``https://example.ch'')
  \item It restricts the application of the following inline style variants:
    \begin{itemize}[itemsep=1pt]
      \item The application of inline styles:\\
        <style> \#someid \{ height: 100px; \} </style>
      \item The application of inline style attributes:\\
        <div style``height: 100px''>
      \item The application of dynamically set inline style attributes:\\
        someElement.setAttribute(``style'', ``height: 100px;'')
    \end{itemize}
  \item It restricts the execution of the following string evaluation variants:
    \begin{itemize}[itemsep=1pt]
      \item The execution of the ``CSSStyleSheet.insertRule'' method:\\
        someStylesheet.insertRule(``\#someid \{ height: 100px; \}'')
      \item The execution of the ``CSSGroupingRule.insertRule'' method:\\
        someGroupingRule.insertRule(``\#someid \{ height: 100px; \}'')
      \item The execution of assignments to the ``CSSStyleDeclaration.cssText'' property:\\
        someElement.style.cssText = ``height: 100px;''
    \end{itemize}
  \item It restricts CSS rules loaded via the ``Link'' HTTP header (not relevant for mobile web applications)
\end{itemize}

\section{worker-src}
The ``worker-src'' directive limits the resources workers can be loaded from. The following Javascript APIs are restricted:
\begin{itemize}[itemsep=1pt]
  \item The Web API's ``Worker'' object:\\
    new Worker(``https://example.ch'')
  \item The Web API's ``SharedWorker'' object:\\
    new SharedWorker(``https://example.ch'')
  \item The Web API's ``navigator.serviceWorker.register'' method:\\
    navigator.serviceWorker.register(``https://example.ch'')
\end{itemize}

\section{base-uri}
The ``base-uri'' directive limits the resources which can be set as the base href for the given page: 
\begin{itemize}[itemsep=1pt]
  \item <base href=``https://example.ch''>
\end{itemize}

\section{plugin-types}
The ``plugin-types'' directive defines the plugins that are allowed to handle data loaded with the HTML tags mentioned under ``object-src''. The following conditions must be met for the given data to be loaded:
\begin{itemize}[itemsep=1pt]
  \item The HTML tag must declare the MIME type of the data:\\
    <object type=``application/x-shockwave-flash'' data=``https://example.ch''>
  \item The data that is loaded by the tag has to actually match the declared MIME type
  \item The declared MIME type must be whitelisted in the ``plugin-types'' directive
\end{itemize}

\section{form-action}
The ``form-action'' directive limits the resources an HTML <form> tag can be submitted to, which is specified in the ``action'' attribute: 
\begin{itemize}[itemsep=1pt]
  \item <form action=``https://example.ch''>
\end{itemize}

\section{upgrade-insecure-requests}
One of the more unusual CSP directives is the ``upgrade-insecure-requests'' directive. Instead of limiting valid resources to connect to, it causes any HTTP request made from the application to be changed to an HTTPS request. For example, the tag <img src=``http://example.ch''> is automatically upgraded by the browser to <img src=``https://example.ch''>. This behavior does not apply to <a> tags, which will navigate to their specified URL, even with this directive set.


