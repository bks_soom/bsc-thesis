\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Related Work}{4}{chapter.2}% 
\contentsline {chapter}{\numberline {3}Security Analysis}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Code Injection}{9}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Injection Sources}{10}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Injection Sinks}{13}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}Impacts of Code Injection}{14}{subsection.3.1.3}% 
\contentsline {section}{\numberline {3.2}Attack Methods for Malicious Apps}{17}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Vetting Circumvention}{17}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Introspection and Manipulation of Third-Party Content}{17}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}User Impersonation}{18}{subsection.3.2.3}% 
\contentsline {subsection}{\numberline {3.2.4}UI Confusion}{18}{subsection.3.2.4}% 
\contentsline {section}{\numberline {3.3}Further Security Flaws}{19}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Exposing Privileged Web Operations via Intents}{19}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Leaking Sensitive Data Through URL Overriding}{19}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}Framework Vulnerabilities}{19}{subsection.3.3.3}% 
\contentsline {section}{\numberline {3.4}Case Study}{19}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}Vulnerability}{20}{subsection.3.4.1}% 
\contentsline {subsection}{\numberline {3.4.2}Impacts}{21}{subsection.3.4.2}% 
\contentsline {chapter}{\numberline {4}Content Security Policy}{23}{chapter.4}% 
\contentsline {section}{\numberline {4.1}How CSP Works}{23}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}CSP Sources}{24}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}CSP Directives}{26}{subsection.4.1.2}% 
\contentsline {section}{\numberline {4.2}How CSP Mitigates Attacks}{27}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}Preventing Code Injection}{27}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Preventing the Loading of Arbitrary Data}{28}{subsection.4.2.2}% 
\contentsline {subsection}{\numberline {4.2.3}Preventing the Exfiltration of Data}{28}{subsection.4.2.3}% 
\contentsline {subsection}{\numberline {4.2.4}Preventing UI Manipulation}{29}{subsection.4.2.4}% 
\contentsline {subsection}{\numberline {4.2.5}Preventing Insecure Connections}{29}{subsection.4.2.5}% 
\contentsline {chapter}{\numberline {5}Retrofitting Cordova Applications for CSP}{30}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Approach}{30}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Processing Related Javascript}{32}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}Setting Constant CSP Directives}{37}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}Extracting Sources from HTML}{37}{subsection.5.1.3}% 
\contentsline {subsection}{\numberline {5.1.4}Extracting Sources from HTML and Related Javascript}{37}{subsection.5.1.4}% 
\contentsline {subsection}{\numberline {5.1.5}Rewriting CSS and Extracting Sources Related to Stylings}{38}{subsection.5.1.5}% 
\contentsline {subsection}{\numberline {5.1.6}Rewriting Scripts and Extracting Sources Related to Them}{39}{subsection.5.1.6}% 
\contentsline {subsection}{\numberline {5.1.7}Writing the Generated CSP Definition to the File}{40}{subsection.5.1.7}% 
\contentsline {section}{\numberline {5.2}Evaluation}{40}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Limiting Patterns Inherent to Content Security Policy}{40}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}Limiting Patterns Inherent to Our Approach}{41}{subsection.5.2.2}% 
\contentsline {subsection}{\numberline {5.2.3}Limiting Patterns Inherent to Specific Use Cases}{43}{subsection.5.2.3}% 
\contentsline {subsection}{\numberline {5.2.4}Further Discoveries}{45}{subsection.5.2.4}% 
\contentsline {subsection}{\numberline {5.2.5}Preliminary Conclusion}{47}{subsection.5.2.5}% 
\contentsline {chapter}{\numberline {6}Empirical Analysis}{49}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Occurrence of Impacted APIs}{50}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Constant vs. Non-Constant Parameters}{50}{section.6.2}% 
\contentsline {section}{\numberline {6.3}Limitations to Rewriting}{51}{section.6.3}% 
\contentsline {section}{\numberline {6.4}Framework Usage}{54}{section.6.4}% 
\contentsline {section}{\numberline {6.5}Threats to Validity}{55}{section.6.5}% 
\contentsline {chapter}{\numberline {7}Conclusion}{56}{chapter.7}% 
\contentsline {chapter}{\numberline {A}Anleitung zu wissenschaftlichen Arbeiten}{59}{appendix.A}% 
\contentsline {section}{\numberline {A.1}The WebView API}{60}{section.A.1}% 
\contentsline {subsection}{\numberline {A.1.1}WebView Class}{60}{subsection.A.1.1}% 
\contentsline {subsection}{\numberline {A.1.2}WebSettings Class}{62}{subsection.A.1.2}% 
\contentsline {subsection}{\numberline {A.1.3}WebViewClient Class}{63}{subsection.A.1.3}% 
\contentsline {section}{\numberline {A.2}Risks Introduced by WebView}{64}{section.A.2}% 
\contentsline {subsection}{\numberline {A.2.1}Code Injection}{64}{subsection.A.2.1}% 
\contentsline {subsection}{\numberline {A.2.2}Lack of Fine-Grained SOP}{65}{subsection.A.2.2}% 
\contentsline {subsection}{\numberline {A.2.3}Bugs Introduced by WebViews}{66}{subsection.A.2.3}% 
\contentsline {section}{\numberline {A.3}Best Practices and Mitigations}{66}{section.A.3}% 
\contentsline {subsection}{\numberline {A.3.1}Use Secure Channels}{66}{subsection.A.3.1}% 
\contentsline {subsection}{\numberline {A.3.2}Do not Display Untrusted Contents}{66}{subsection.A.3.2}% 
\contentsline {subsection}{\numberline {A.3.3}Sanitize Untrusted Inputs}{67}{subsection.A.3.3}% 
\contentsline {subsection}{\numberline {A.3.4}Use Safe APIs to Display Untrusted Contents}{67}{subsection.A.3.4}% 
\contentsline {subsection}{\numberline {A.3.5}Prevent Navigation}{67}{subsection.A.3.5}% 
\contentsline {subsection}{\numberline {A.3.6}Prevent New Windows and Popups}{67}{subsection.A.3.6}% 
\contentsline {subsection}{\numberline {A.3.7}Handle SSL Errors Safely}{67}{subsection.A.3.7}% 
\contentsline {subsection}{\numberline {A.3.8}Use JS Bridges with Care}{67}{subsection.A.3.8}% 
\contentsline {subsection}{\numberline {A.3.9}Use Content Security Policy}{68}{subsection.A.3.9}% 
\contentsline {subsection}{\numberline {A.3.10}Update Regularly}{68}{subsection.A.3.10}% 
\contentsline {subsection}{\numberline {A.3.11}Use Additional Security Measures}{69}{subsection.A.3.11}% 
\contentsline {chapter}{\numberline {B}CSP Directives}{70}{appendix.B}% 
\contentsline {section}{\numberline {B.1}connect-src}{70}{section.B.1}% 
\contentsline {section}{\numberline {B.2}default-src}{71}{section.B.2}% 
\contentsline {section}{\numberline {B.3}font-src}{71}{section.B.3}% 
\contentsline {section}{\numberline {B.4}frame-src}{72}{section.B.4}% 
\contentsline {section}{\numberline {B.5}img-src}{72}{section.B.5}% 
\contentsline {section}{\numberline {B.6}manifest-src}{72}{section.B.6}% 
\contentsline {section}{\numberline {B.7}media-src}{72}{section.B.7}% 
\contentsline {section}{\numberline {B.8}object-src}{73}{section.B.8}% 
\contentsline {section}{\numberline {B.9}script-src}{73}{section.B.9}% 
\contentsline {section}{\numberline {B.10}style-src}{74}{section.B.10}% 
\contentsline {section}{\numberline {B.11}worker-src}{74}{section.B.11}% 
\contentsline {section}{\numberline {B.12}base-uri}{75}{section.B.12}% 
\contentsline {section}{\numberline {B.13}plugin-types}{75}{section.B.13}% 
\contentsline {section}{\numberline {B.14}form-action}{75}{section.B.14}% 
\contentsline {section}{\numberline {B.15}upgrade-insecure-requests}{75}{section.B.15}% 
